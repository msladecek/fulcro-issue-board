(ns todo-app.person-list
  (:require
   [clojure.pprint :refer [pprint]]
   [com.fulcrologic.fulcro.application :as app]
   [com.fulcrologic.fulcro.components :as comp :refer [defsc]]
   [com.fulcrologic.fulcro.dom :as dom]
   [com.fulcrologic.fulcro.algorithms.merge :as merge]
   [com.fulcrologic.fulcro.mutations :as m :refer [defmutation]]))

(defmutation delete-person
  [{list-id :list/id, person-id :person/id}]
  (action [{:keys [state]}]
          (swap! state merge/remove-ident*
                 [:person/id person-id]
                 [:list/id list-id :list/people])))


(defsc Person [this {:person/keys [id name age] :as props} {:keys [onDelete]}]
  {:query [:person/id :person/name :person/age]
   :ident (fn [] [:person/id (:person/id props)])
   :initial-state {:person/id :param/id
                   :person/name :param/name
                   :person/age :param/age}}
  (dom/li
   (dom/h5 (str name "(age: " age ")")
           (dom/button {:onClick #(onDelete id)} "X"))))

(def ui-person (comp/factory Person {:keyfn :person/name}))


(defsc PersonList [this {:list/keys [id label people] :as props}]
  {:query [:list/id :list/label {:list/people (comp/get-query Person)}]
   :ident (fn [] [:list/id (:list/id props)])
   :initial-state
   (fn [{:keys [id label]}]
     {:list/id id
      :list/label label
      :list/people (if (= id :friends)
                     [(comp/get-initial-state Person {:id 1 :name "Sally" :age 32})
                      (comp/get-initial-state Person {:id 2 :name "Joe" :age 22})]
                     [(comp/get-initial-state Person {:id 3 :name "Fred" :age 11})
                      (comp/get-initial-state Person {:id 4 :name "Bobby" :age 55})])})}
  (let [delete-person-handler
        (fn [person-id]
          (comp/transact! this [(delete-person {:list/id id :person/id person-id})]))]
    (dom/div
     (dom/h4 label)
     (dom/ul
      (map
       (fn [person] (ui-person (comp/computed person {:onDelete delete-person-handler})))
       people)))))

(def ui-person-list (comp/factory PersonList))
