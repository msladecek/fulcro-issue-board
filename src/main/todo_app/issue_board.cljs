(ns todo-app.issue-board
  (:require
   [clojure.pprint :refer [pprint]]
   [com.fulcrologic.fulcro.application :as app]
   [com.fulcrologic.fulcro.components :as comp :refer [defsc]]
   [com.fulcrologic.fulcro.dom :as dom]
   [com.fulcrologic.fulcro.mutations :as m :refer [defmutation]]))


(defmutation change-issue-state
  [{issue-id :issue/id new-state :issue/state :as params}]
  (action [{:keys [state]}]
          (swap! state assoc-in [:issue/id issue-id :issue/state] new-state)))


(defn state-id->state-label [state-id]
  (-> state-id name clojure.string/upper-case))


(defsc Issue [this {:issue/keys [id label state priority]} {:keys [change-state]}]
  {:query [:issue/id :issue/label :issue/state :issue/priority]
   :ident (fn [] [:issue/id id])
   :initial-state {:issue/id :param/id
                   :issue/label :param/label
                   :issue/state :param/state
                   :issue/priority :param/priority}}
  (let [issue-transitions {:todo [:in-progress]
                           :in-progress [:todo :done]
                           :done [:in-progress]}]
    (dom/div
     :.ui.card
     {:style {:padding "0.5em"
              :width "100%"}}
     (dom/h5 label)
     (dom/div
      {:style {:display "flex"}}
      (mapv
       (fn [target-state]
         (dom/button
          :.ui.button
          {:onClick #(change-state id target-state)
           :style {:width "100%"}}
          (str "move to " (state-id->state-label target-state))))
       (get issue-transitions state))))))

(def ui-issue (comp/factory Issue))


(defsc IssueBoard [this {:issue-board/keys [id columns issues] :as props}]
  {:query [:issue-board/id :issue-board/columns {:issue-board/issues (comp/get-query Issue)}]
   :ident (fn [] [:issue-board/id (:issue-board/id props)])
   :initial-state (fn [{:keys [id]}]
                    {:issue-board/id id
                     :issue-board/columns [:todo :in-progress :done]
                     :issue-board/issues
                     (mapv (fn [issue-map] (comp/get-initial-state Issue issue-map))
                           [{:id 1 :label "Kill batman" :state :todo :priority 100}
                            {:id 2 :label "Release laughing gas" :state :in-progress :priority 10}
                            {:id 3 :label "Escape asylum" :state :done :priority 1000}
                            {:id 4 :label "Blow up the batcave" :state :todo :priority 0}
                            {:id 5 :label "Recruit henchmen" :state :in-progress :priority 0}
                            {:id 6 :label "Buy a present for Alfred's birthday" :state :todo :priority 100}
                            {:id 7 :label "Do master Wayne's laundry" :state :todo :priority 2000}])})}
  (let [issues-by-state (->> issues
                             (sort-by :issue/priority >)
                             (group-by :issue/state))
        change-state-handler
        (fn [issue-id new-state]
          (comp/transact! this [(change-issue-state {:issue/id issue-id :issue/state new-state})]))]
    (dom/div
     {:style {:display "flex"
              :width "100%"}}
     (for [state-id columns]
       (let [state-label (state-id->state-label state-id)
             column-issues (issues-by-state state-id)]
         (dom/div
          :.ui.card
          {:style {:flex 1
                   :padding "0.5em"
                   :margin "0.5em"
                   :display "flex"}}
          (dom/h3 state-label)
          (mapv (fn [issue]
                  (ui-issue (comp/computed issue {:change-state change-state-handler})))
                column-issues)))))))

(def ui-issue-board (comp/factory IssueBoard))
