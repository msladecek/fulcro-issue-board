(ns todo-app.client
  (:require
   [todo-app.person-list :refer [ui-person-list PersonList]]
   [todo-app.issue-board :refer [ui-issue-board IssueBoard]]
   [clojure.pprint :refer [pprint]]
   [com.fulcrologic.fulcro.application :as app]
   [com.fulcrologic.fulcro.components :as comp :refer [defsc]]
   [com.fulcrologic.fulcro.dom :as dom]
   [com.fulcrologic.fulcro.mutations :as m :refer [defmutation]]))


(defonce app (app/fulcro-app))


(defsc Root [this {:keys [friends enemies issues]}]
  {:query [{:friends (comp/get-query PersonList)}
           {:enemies (comp/get-query PersonList)}
           {:issues (comp/get-query IssueBoard)}]
   :initial-state (fn [params]
                    {:friends (comp/get-initial-state PersonList {:id :friends :label "Friends"})
                     :enemies (comp/get-initial-state PersonList {:id :enemies :label "Enemies"})
                     :issues (comp/get-initial-state IssueBoard {:id :main-board})})}
  (dom/div
   (dom/h2 "People")
   (dom/div
    (ui-person-list friends)
    (ui-person-list enemies))
   (dom/h2 "Issue Board App")
   (ui-issue-board issues)))


(defn ^:export init []
  (app/mount! app Root "app")
  (js/console.log "load"))


(defn ^:export refresh []
  (app/mount! app Root "app")
  (js/console.log "hot reload"))
